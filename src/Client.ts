import { Client as DiscordClient, ClientOptions as DiscordClientOptions, Interaction } from 'discord.js';

import { GuildCommandManager, ListenerManager, ModuleManger } from './managers';

export class Client extends DiscordClient {
    public readonly moduleManager: ModuleManger;
    public readonly listenerManager: ListenerManager;
    public readonly guildCommandManager: GuildCommandManager;

    constructor(options: DiscordClientOptions) {
        super(options);

        this.moduleManager = new ModuleManger(this);
        this.listenerManager = new ListenerManager(this);
        this.guildCommandManager = new GuildCommandManager(this);

        this.on('interactionCreate', async (interaction: Interaction) => {
            if (!interaction.isCommand()) return;

            const command = this.guildCommandManager.registry.find(c => c.options.data.name === interaction.commandName);

            if (command) {
                try {
                    await command.execute(interaction);
                } catch (err) {
                    console.error(err);
                    await interaction.reply('An unexpected error occurred during the execution of this command,' +
                        ' please contact an administrator.')
                        .catch(err => console.error(err));
                }
            }
        });
    }
}
