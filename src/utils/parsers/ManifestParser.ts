import { Schema, Validator } from 'jsonschema';

// Notable resources:
//      https://github.com/tdegrunt/jsonschema
//      https://ajv.js.org/guide/typescript.html

// todo: Replace hard-coded schema with configurable json file.

export class ManifestParser {
    private manifestSchema: Schema = {
        'id': '/manifest',
        'type': 'object',
        'properties': {
            'name': {
                'type': 'sting'
            },
            'version': {
                'type': 'string'
            },
            'description': {
                'type': 'string'
            }
        },
        'required': [
            'name',
            'version'
        ]
    };

    private jsonValidator: Validator;

    public constructor() {
        this.jsonValidator = new Validator();
    }

    public validate(manifestJson: any) {
        return this.jsonValidator.validate(manifestJson, this.manifestSchema);
    }

    public isValid(manifestJson: any): boolean {
        return this.jsonValidator.validate(manifestJson, this.manifestSchema).valid;
    }
}
