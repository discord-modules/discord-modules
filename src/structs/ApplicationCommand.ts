import { CommandInteraction } from 'discord.js';
import { SlashCommandBuilder } from '@discordjs/builders';

import { Module } from './Module';

export interface ApplicationCommandOptions {
    data: SlashCommandBuilder;
}

export abstract class ApplicationCommand {
    protected constructor(public readonly module: Module, public readonly options: ApplicationCommandOptions) {
    }

    abstract execute(interaction: CommandInteraction): Promise<void>;
}
