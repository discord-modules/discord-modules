import { ClientEvents } from 'discord.js';

import { Module } from './Module';

export abstract class Listener<K extends keyof ClientEvents> {
    public readonly module: Module;
    public readonly event: K;

    protected constructor(module: Module, event: K) {
        this.module = module;
        this.event = event;
    }

    abstract execute(...args: ClientEvents[K]): Promise<void>;
}
