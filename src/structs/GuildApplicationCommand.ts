import { Guild } from 'discord.js';

import { ApplicationCommand, ApplicationCommandOptions, Module } from '../structs';

export interface GuildApplicationCommandOptions extends ApplicationCommandOptions {
    guild: Guild;
}

export abstract class GuildApplicationCommand extends ApplicationCommand {
    protected constructor(module: Module, public override readonly options: GuildApplicationCommandOptions) {
        super(module, options);
    }
}