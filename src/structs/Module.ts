import { Client } from '../Client';

export interface ModuleManifest {
    name: string,
    version: string,
    description?: string,
    insights: string[]
}

export abstract class Module {
    protected constructor(public readonly client: Client, public readonly manifest: ModuleManifest) {
    }

    abstract register(): Promise<void>;
}
