import { Collection } from 'discord.js';

import { Client, Module } from '..';

export class ModuleManger {
    public readonly registry: Collection<string, Module>;

    constructor(public readonly client: Client) {
        this.registry = new Collection<string, Module>();
    }

    /**
     * Register a specific module. Likely used to register module's commands and listeners.
     *
     * @param module Module to be registered.
     * @returns {Promise<void>}
     */
    public register(module: Module): Promise<void> {
        return new Promise<void>(async (resolve, reject) => {
            const moduleName = module.manifest.name.toLowerCase();

            if (this.registry.has(moduleName)) {
                return reject(new Error(`A module is registered with the name '${ moduleName }'.`));
            }

            try {
                await module.register();
            } catch (err) {
                return reject(err);
            }

            this.registry.set(moduleName, module);
            return resolve();
        });
    }
}