import { ClientEvents } from 'discord.js';

import { Listener, Client } from '../';

export class ListenerManager {
    public readonly registry: Array<Listener<keyof ClientEvents>>;

    constructor(public readonly client: Client) {
        this.registry = new Array<Listener<keyof ClientEvents>>();
    }

    public register(listener: Listener<keyof ClientEvents>): Promise<void> {
        return new Promise<void>((resolve, reject) => {
            // todo: Log register listener operation.

            this.client.on(listener.event, (...args) => listener.execute(...args));
            this.registry.push(listener);
            console.log(`Registered listener global '${listener.event}'.`);
            return resolve();
        });
    }
}