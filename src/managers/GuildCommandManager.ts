import { Guild } from 'discord.js';
import { Client, GuildApplicationCommand } from '..';

const { REST } = require('@discordjs/rest');
const { Routes } = require('discord-api-types/v9');

export class GuildCommandManager {
    public readonly registry: Array<GuildApplicationCommand>;

    constructor(public readonly client: Client) {
        this.registry = new Array<GuildApplicationCommand>();
    }

    public register(command: GuildApplicationCommand): Promise<void> {
        return new Promise<void>(async (resolve, reject) => {
            if (this.registry.find(c => c.options.guild === command.options.guild && c.options.data.name === command.options.data.name)) {
                return reject(new Error(`The command '${ command.options.data.name }' has already been registered in the guild scope '${ command.options.guild.id }'.`));
            }

            this.registry.push(command);
            console.log(`Queued command '${ command.options.data.name }' to be registered in guild '${ command.options.guild.id }'.`);
            return resolve();
        });
    }

    public refreshGuildCommands(guild: Guild): Promise<void> {
        return new Promise(async (resolve, reject) => {
            if (this.registry.length == 0) {
                console.log(`No commands queued for registration. Skipping guild command refresh.`);

                return resolve();
            }

            const rest = new REST({ version: '9' }).setToken(process.env.DISCORD_APPLICATION_TOKEN);
            const commands = this.registry.filter(c => c.options.guild.id === guild.id).map(c => c.options.data);

            try {
                await rest.put(Routes.applicationGuildCommands(process.env.DISCORD_APPLICATION_ID, guild.id), {
                    body: this.registry.filter(c => c.options.guild.id === guild.id).map(c => c.options.data)
                }).catch(console.error);

                for (const command of commands) {
                    console.log(`Registered command '${command.name}' in guild '${guild.id}'.`);
                }

                return resolve();
            } catch (err) {
                return reject(err);
            }
        });
    }
}